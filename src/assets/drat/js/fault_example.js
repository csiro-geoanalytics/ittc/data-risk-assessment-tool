

$(document).ready(function () {
    // Start writing your custom functions here.
    // All the necessary pluigns are already loaded.

    !function PrepareFileList() {
		
        $('#tblDSResults').DataTable({
            ajax: {
                url: siteURL + "/?action=populate_files_table",
                dataSrc: ""
            },
            columns: [
                {title: "Dataset name", data: 0, width: "60%"},
                {title: "Filetype", data: 1, width: "10%"},
                {title: "Fileset size (kB)", data: 2, width: "10%"},
                {title: "Average rating", data: 3, width: "10%"},
                {title: "DSID", data: 4, visible: false}
            ],
            autoWidth: true,
            ordering: true,
            order: [[2, 'dsc'], [0, 'asc']],
            scrollY: "400px",
            scrollCollapse: true,
            hover: true,
            select: {
                item: 'row',
                style: 'single',
                info: false
            },
            language: {
                search: 'Filter these records:',
                info: "_TOTAL_ datasets found : Showing datasets _START_ to _END_ ",
                lengthMenu: 'Display <select>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">All</option>' +
                        '</select> datasets'
            }
        });
    }();


    !function PrepareTabs() {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust().draw();
            $.fn.dataTable.tables({visible: true, api: true}).responsive.recalc();
			console.log("click tab nav")
        });
    }();


  
    !function GetFileMetaData() {
		
        var dsid = GetTableRowID();
        if (Boolean(dsid) == false)
        {
            dsid = 0;
        }
        ;  //if no row is selected, set dsid to 0.  Only field names will be returned.	

        $('#tblFileMetaData').DataTable({
            ajax:
                    {
                        url: siteURL + "/?action=populate_file_info_table&dsid=" + dsid,
                        dataSrc: ""
                    },
            columns: [
                {data: 0, width: "30%", cellType: "th"},
                {data: 1, width: "70%"}
            ],
            search: false,
            autoWidth: false,
            ordering: false,
            paging: false,
            info: false,
            searching: false,
            stateSave: true,
            language: {
                lengthMenu: "",
                zeroRecords: "No file selected",
                infoEmpty: "No file selected"
            },
            dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            sPaginationType: "bootstrap"
        });

    }();
	
    !function OnRowClick() {
		
		$('#tblDSResults').on('click', 'tr', function () {
			
     		var id = GetTableRowID();
			
			if (Boolean(id) == false)
                id = 0;
 			
			var table6 = $('#tblMFA').DataTable();
			table6.destroy;
			
			getMFData(id);
			
		});
    }();

   function GetTableRowID() {
		var table = $('#tblDSResults').DataTable();
		var id = table.cell(table.row('.selected'), 4).data();
		if (Boolean(id) == false)
			{
				id = 0;
			};
		console.log("dsid=" + id);
		return id;
	};

	function getMFData(dsid) {
		var columns = [];
		
		//setup table for all of the details for Multi-file datasets
	
		$.ajax({
		  url: siteURL + '/?action=populate_fileattributes_table&dsid=' + dsid,
		  dataType: 'json',
		  success: function (data){
			var data = data.data;
			console.log("rows of data =" + data.length);
			if (data.length === 0) { //an empty recordset
				columns = [null];
				data=[];
				} else {									
					var columnNames = Object.keys(data[0]);
					for (var i in columnNames) {
						columns[i]={
							title: columnNames[i],
							data: columnNames[i]
						};						
					}
				}
			console.log(siteURL + '/?action=populate_fileattributes_table&dsid=' + dsid);	
			console.log(data);
			console.log(columns);
			
			$('#tblMFA').DataTable({
				data:data,
				columns:columns,
				language: {
					lengthMenu: "",
					zeroRecords: "No data available"
						},
				paging:true,
				scrollX:true,
				scrossY:true,
				searching: false,
				autoWidth: false,
				ordering: false /*,
				drawCallback: function ( ) {
					$.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
					}	*/
				}) ;						
			},
			error: function(){
				console.log("problem here");
				}
		});
	};

	  
   !function  GetFileAttributes() {
        var dsid = GetTableRowID();
        if (Boolean(dsid) == false)
        {
            dsid = 0;
        }
        ;  //if no row is selected, set dsid to 0.  

		console.log("Setting up multi file attributes for the first time");
        //setup table for all of the details for Multi-file datasets
		 
		getMFData(dsid);

		
    }();
});
