

$(document).ready(function () {
    // Start writing your custom functions here.
    // All the necessary pluigns are already loaded.

    !function PrepareFileList() {
		
        $('#tblDSResults').DataTable({
            ajax: {
                url: siteURL + "/?action=populate_files_table",
                dataSrc: ""
            },
            columns: [
                {title: "Dataset name", data: 0, width: "70%", render: function(data,type,row){ return '<span itemscope itemtype="http://schema.org/Dataset" itemprop="name" > ' + data + '</span>'} ,},
                {title: "Filetype", data: 1, width: "10%"},
                {title: "Fileset size (kB)", data: 2, width: "10%"},
                {title: "Average rating", data: 3, width: "10%"},
                {title: "DSID", data: 4, visible: false}
            ],
            autoWidth: true,
            ordering: true,
            order: [[2, 'dsc'], [0, 'asc']],
            scrollY: "400px",
            scrollCollapse: true,
            hover: true,
            select: {
                item: 'row',
                style: 'single',
                info: false
            },
            language: {
                search: 'Filter these records:',
                info: "_TOTAL_ datasets found : Showing datasets _START_ to _END_ ",
                lengthMenu: 'Display <select>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">All</option>' +
                        '</select> datasets'
            }
        });
    }();


    !function PrepareTabs() {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust().draw();
            $.fn.dataTable.tables({visible: true, api: true}).responsive.recalc();
			console.log("click tab nav");
        });
    }();


  
    !function GetFileMetaData() {
		
        var dsid = GetTableRowID();
        if (Boolean(dsid) == false)
        {
            dsid = 0;
        }
        ;  //if no row is selected, set dsid to 0.  Only field names will be returned.	

        $('#tblFileMetaData').DataTable({
            ajax:
                    {
                        url: siteURL + "/?action=populate_file_info_table&dsid=" + dsid,
                        dataSrc: ""
                    },
            columns: [
                {data: 0, width: "30%", cellType: "th"},
                {data: 1, width: "70%"}
            ],
            search: false,
            autoWidth: false,
            ordering: false,
            paging: false,
            info: false,
            searching: false,
            stateSave: true,
            language: {
                lengthMenu: "",
                zeroRecords: "No file selected",
                infoEmpty: "No file selected"
            },
            dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            sPaginationType: "bootstrap"
        });

    }();

	
    !function OnRowClick() {
		
		$('#tblDSResults').on('click', 'tr', function () {
			
			var downloadBtn = $("#downloadBtn");
			downloadBtn.hide();
            
			var id = GetTableRowID();
            
			var table1 = $('#tblFileMetaData').DataTable();
            var table2 = $('#tblEquipMetaData').DataTable();
            //var table3 = $('#tblColumnInfo').DataTable();		
            var table4 = $('#tblEquipParameters').DataTable();
			var table5 = $('#tblDatasetDetails').DataTable();       
			
			table1.ajax.url(siteURL + '/?action=populate_file_info_table&dsid=' + id).load();
            table2.ajax.url(siteURL + '/?action=populate_source_metadata_table&dsid=' + id).load();
			//table3.ajax.url(siteURL + '/?action=populate_column_info_table&dsid=' + id).load();
            table4.ajax.url(siteURL + '/?action=populate_equip_parameters_table&dsid=' + id).load();
			table5.ajax.url(siteURL + '/?action=populate_dataset_details_table&dsid=' + id).load();
			
			UpdateColInfo2(id);
			getMFData(id);
			
            // load comments
            load_user_comments(1);
            
            // change sample file
  				
			downloadBtn.attr("href", "download_file?fileid=" + id);
            downloadBtn.show();     
				
            
			
			
			//var cell = $('#tblDSResults.selected td:first-child').html();
			//console.log(cell);
		});
    }();

    function GetTableRowID() {
		var table = $('#tblDSResults').DataTable();
		var id = table.cell(table.row('.selected'), 4).data();
		if (Boolean(id) == false)
			{
				id = 0;
			};
		//console.log("dsid=" + id);
		return id;
}

	function getMFData(dsid) {
	  var columns = [];
		
	//setup table for all of the details for Multi-file datasets
	  $.ajax({
		url: siteURL + "/?action=populate_fileattributes_table&dsid=" + dsid,
		dataType: "json",
		success: function(data) {
		  var data = data.data;
		  // Check if there is an existing DataTable and if so, destroy it
		  if ($.fn.dataTable.isDataTable("#tblMFA")) {			
			$("#tblMFA")
			  .DataTable()
			  .destroy();		
			$("#tblMFA").empty()			  
		  }
		
		  // Remove any `no-data` message from a previous table
		  $("#tblMFA")
			   .parent()
			   .children("div.no-data")
			   .remove();
			   
		  if (data.length === 0) {
			// No data, so show a message to that effect
			
			$("#tblMFA")
			   .css("display", "none")
			   .after('<div class="no-data extra_space_below">No parameters to display </div>');
		  
		  } else {
			
	 
			// Build the DataTables columns object from the property names in the data object
			var columns = Object.keys(data[0]).map(function(val) {
			  return {
				title: val,
				data: val
			  };
			});
			/*
			console.log("columns array:");
			console.log(columns);
			console.log("data array:");
			console.log(data);*/
			$("#tblMFA").css('display', 'block');
			
			$("#tblMFA").DataTable({		    
				data:data,
			    columns: columns,
			    language: {
				  lengthMenu: "",
				  zeroRecords: "No parameters to display."
			    },
			    paging: true,
			    pageLength:20,
			    scrollX: true,
				scrollY:true,
			    searching: false,
			    autoWidth: false,
			    ordering: false,
			    drawCallback: function() {
				  $.fn.dataTable.tables({visible:true,api:true}).columns.adjust();
				}
			}); //end of Table setup
		  }//End of else (data.length<0) statement
		},//End of success condition
		error: function() {
		   console.log("problem here");
		} //End of error condition
	  }); //End of $.ajax statement
	};//end of function



    !function GetEquipMetaData() {
		//$val = "hi";
			
        var dsid = GetTableRowID();
		//console.log(" Getting Equipment MetaData");
        
        //alert(siteURL +'/?action=populate_source_metadata_table&dsid=' + dsid);
        $('#tblEquipMetaData').DataTable({
			
            ajax:
                    {
                        url: siteURL + "/?action=populate_source_metadata_table&dsid=" + dsid,
                        dataSrc: ""
                    },
            columns: [
                {data: 0, width: "40%", cellType: "th"},
                {data: 1, width: "60%"}
            ],
            paginationType: 'bootstrap',
            search: false,
            autoWidth: false,
            ordering: false,
            scrollY: "1000px",
            scrollCollapse: true,
            paging: false,
            info: false,
            searching: false,
            stateSave: true,
            language: {
                lengthMenu: "",
                zeroRecords: "No file selected",
                infoEmpty: "No file selected"
            }
        });
		
		
    }();

    
	
    !function GetDatasetDetails() {
 		var dsid=GetTableRowID();
        //console.log("Getting dataset info");
        $('#tblDatasetDetails').DataTable({
            ajax:
                    {
                        url: siteURL + "/?action=populate_dataset_details_table&dsid=" + dsid,
                        dataSrc: ""
                    },
            columns: [
                {data: 0, width: "40%", cellType: "th"},
                {data: 1, width: "60%"}
            ],
            paginationType: 'bootstrap',
            search: false,
            autoWidth: false,
            ordering: false,
            scrollY: "1000px",
            scrollCollapse: true,
            paging: false,
            info: false,
            searching: false,
            stateSave: true,
            language: {
                lengthMenu: "",
                zeroRecords: "No file selected",
                infoEmpty: "No file selected"
            }
        });
        var table = $('#tblDatasetDetails').DataTable();

    }();
	
    
	!function GetColumnInfo() {
		UpdateColInfo2(0);
	}();
	
	
	function UpdateColInfo2(dsid) {
		
		var columns = [];
		
	//setup table for all of the details for Multi-file datasets
	  $.ajax({
		url: siteURL + "/?action=populate_column_info_table&dsid=" + dsid,
		dataType: "json",
		success: function(data) {
		  
		  var data = data.data;
		  // Check if there is an existing DataTable and if so, destroy it
		  
		  if ($.fn.dataTable.isDataTable("#tblColumnInfo")) {			
			$("#tblColumnInfo").DataTable().destroy();		
			$("#tblColumnInfo").empty();			  
		  }
		  
		  // Remove any `no-data` message from a previous table
		  $("#tblColumnInfo")
			   .parent()
			   .children("div.no-data")
			   .remove();
			   
		  if (data.length === 0) {
			// No data, so show a message to that effect
			
			$("#tblColumnInfo")
			   .css("display", "none")
			   .after('<div class="no-data extra_space_below">No parameters to display </div>');
		  
		  } else {
			
	 
			// Build the DataTables columns object from the property names in the data object
			var columns = Object.keys(data[0]).map(function(val) {
			  return {
				title: val,
				data: val
			  };
			});
			
			console.log("columns array:");
			console.log(columns);
			console.log("data array:");
			console.log(data);
			$("#tblColumnInfo").css('display', 'block');
			
			$("#tblColumnInfo").DataTable({		    
				destroy:true,
				data:data,
			    columns: columns,
			    language: {
				  lengthMenu: ""
			    },
			    paging: true,
			    pageLength:20,
			    scrollX: true,
				scrollY:true,
				fixedColumns: {
					leftColumns:2
				},
				searching: false,
			    autoWidth: false,
			    ordering: false,
			    drawCallback: function() {
				  $.fn.dataTable.tables({visible:true,api:true}).columns.adjust();
				}
			}); //end of Table setup
		  }//End of else (data.length<0) statement
		},//End of success condition
		error: function() {
		   console.log("problem here");
		} //End of error condition
	  }); //End of $.ajax statement
	};	
	
    !function GetEquipParameters() {
        var dsid = GetTableRowID();
        
		//console.log("Getting EquipParameters");
		
        $('#tblEquipParameters').DataTable({
            ajax:
                {
                    url: siteURL + "/?action=populate_equip_parameters_table&dsid=" + dsid,
                    dataSrc: ""
                },
            columns: [
                {data: 0, width: "40%", cellType: "th"},
                {data: 1, width: "60%"}
            ],
            paginationType: 'bootstrap',
            search: false,
            autoWidth: false,
            ordering: false,
            scrollY: "1000px",
            scrollCollapse: true,
            paging: false,
            info: false,
            searching: false,
            stateSave: true,
            fixedHeader: {
                leftColumns: 2
            },
            language: {
                lengthMenu: "",
                zeroRecords: "No data available",
                infoEmpty: "No file selected"
            },
            drawCallback: function ( ) {
                $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
            }
        });
        var table = $('#tblEquipParameters').DataTable();
        table.column(0).visible(true);

    }();

				 
	  
   !function  GetFileAttributes() {
        var dsid = GetTableRowID();
        

		console.log("Getting multi file attributes");
        //setup table for all of the details for Multi-file datasets
		 
		getMFData(dsid);

		
    }();



	
    // load comments
	
    function load_user_comments (page) {
        var dsid = GetTableRowID();
        if (Boolean(dsid) == false) { // if no row is selected
            // show "empty" message
            $('#average_ratings_and_comments').hide();
            $('.no_file_selected').show();
        } else {
            // show data
            $.get(siteURL + "/comments", {dsid:dsid, page:page})
                    .done(function(response) {
                        var obj = jQuery.parseJSON(response);
                
                        if (obj.error) {
                            // show error message
                            //$("#js-error-message").html(obj.error_message);
                            //$(".error-message").show();
                        } else {
                            // 1. update stars
                            $('#stars_quality_of_data').barrating('set', obj.statistic['AvgDQ']);
                            $('#stars_quality_of_meta_data').barrating('set', obj.statistic['AvgMDQ']);
                            $('#stars_suitability_for_prognostics').barrating('set', obj.statistic['AvgSuit']);

                            $('#comments_amount').html(obj.pagination.total_comments);
                            
                            var comments_html = '';
                            
                            if (obj.comments.length>0) {
                                comments_html += '<ul class="media-list">';
                                
                                $.each(obj.comments, function() {
                                    
                                    comments_html += '\
                                    <li class="media">\
                                        <div class="media-left">\
                                            <img class="media-object avatar img-circle" src="http://www.gravatar.com/avatar/'+this.user.Email+'&s=120" alt="'+this.user.Login+'">\
                                        </div>\
                                        <div class="media-body">\
                                            <div class="likes_and_date"><span id="counted_likes_'+this.Comment_ID+'">'+(this.Likes==1?'1 like':this.Likes+' likes')+'</span> | '+this.CommentDate+'</div>\
                                            <h4 class="media-heading">'+this.user.Login+'</h4>\
                                            '+this.Comment+'\
                                            '+(this.PrognosticsUse ? '<div class="used_file_to_model">'+this.user.Login+' used this file for: '+this.PrognosticsUse+'</div>':'')+'\
                                            <div class="comment_actions">'+(this.liked_already?'Liked':'<a class="link js-like-toggle" data-id="'+this.Comment_ID+'"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Like</a>')+' | <a class="link js-reply-link" data-id="'+this.Comment_ID+'"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a> | '+(this.replies>0?'<a class="link js-load-replies" data-id="'+this.Comment_ID+'">'+(this.replies=='1'?'1 Reply':this.replies+' Replies')+'</a>':'No Replies')+'</div>\
                                            <div class="reply_form_placeholder" id="reply_form_placeholder_'+this.Comment_ID+'"></div>\
                                            <div id="replies_container_'+this.Comment_ID+'"></div>\
                                        </div>\
                                    </li>\
                                    ';
                                    
                                });

                                comments_html += '</ul>';
                                
                                // pagination
                                if (obj.pagination.total_pages>1) {
                                    
                                    var pagination_html = '\
                                        <nav class="text-right">\
                                            <ul class="pagination">\
                                                <li class="'+(obj.pagination.current_page=='1'?'disabled':'')+'"><a class="link js-comments-pagination" data-id="'+(parseInt(obj.pagination.current_page)-1)+'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                                    
                                    for (var i=1; i<=obj.pagination.total_pages ; i++) {
                                        pagination_html += '\
                                            <li '+(obj.pagination.current_page==i?'class="active"':'')+'><a class="link js-comments-pagination" data-id="'+i+'">'+i+'</a></li>\
                                        ';
                                    }
                                    
                                    pagination_html += '\
                                            <li class="'+(obj.pagination.current_page==obj.pagination.total_pages?'disabled':'')+'"><a class="link js-comments-pagination" data-id="'+(parseInt(obj.pagination.current_page)+1)+'" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>\
                                            </ul>\
                                        </nav>\
                                    ';
                                    
                                    comments_html += pagination_html;
                                    
                                }
                            } else {
                                //comments_html = "No comments yet";
                            }

                            $('#comments_container').html(comments_html);
                            
                        }
                    });
            
            $('.no_file_selected').hide();
            $('#average_ratings_and_comments').show();
            
        }
                
    };
    
    // load replies
        
    $(document).on("click", ".js-load-replies", function() {
       
        var dsid = GetTableRowID();
        var pid = $(this).attr('data-id');
        
        if ($('#replies_container_'+pid).html()!=='') $('#replies_container_'+pid).html('');
        else {
        


            // show data
            $.get(siteURL + "/", {action:'replies', dsid:dsid, pid:pid})
                .done(function(response) {
                    var obj = jQuery.parseJSON(response);

                    if (obj.error) {
                        // show error message
                        //$("#js-error-message").html(obj.error_message);
                        //$(".error-message").show();
                    } else {

                        var comments_html = '';

                        if (obj.comments.length>0) {
                            comments_html += '<ul class="media-list">';

                            $.each(obj.comments, function() {

                                comments_html += '\
                                <li class="media">\
                                    <div class="media-left">\
                                        <img class="media-object avatar img-circle" src="http://www.gravatar.com/avatar/'+this.user.Email+'&s=120" alt="'+this.user.FullName+'">\
                                    </div>\
                                    <div class="media-body">\
                                        <div class="likes_and_date"><span id="counted_likes_'+this.Comment_ID+'">'+(this.Likes==1?'1 like':this.Likes+' likes')+'</span> | '+this.CommentDate+'</div>\
                                        <h4 class="media-heading">'+this.user.FullName+'</h4>\
                                        '+this.Comment+'\
                                        '+(this.PrognosticsUse ? '<div class="used_file_to_model">'+this.user.FullName+' used this file to model: '+this.PrognosticsUse+'</div>':'')+'\
                                        <div class="comment_actions">'+(this.liked_already?'Liked':'<a class="link js-like-toggle" data-id="'+this.Comment_ID+'"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Like</a>')+' | <a class="link js-reply-link" data-id="'+this.Comment_ID+'"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a> | '+(this.replies>0?'<a class="link js-load-replies" data-id="'+this.Comment_ID+'">'+(this.replies=='1'?'1 Reply':this.replies+' Replies')+'</a>':'No Replies')+'</div>\
                                        <div class="reply_form_placeholder" id="reply_form_placeholder_'+this.Comment_ID+'"></div>\
                                        <div id="replies_container_'+this.Comment_ID+'"></div>\
                                    </div>\
                                </li>\
                                ';

                            });

                            comments_html += '</ul>';

                        } else {
                            //comments_html = "No comments yet";
                        }

                        $('#replies_container_'+pid).html(comments_html);

                    }
                });
                
        }

    });
    
    // load users comments from 1st page
    load_user_comments(1);
    
    // upgrade textareas with autogrow heights
    autosize($('textarea'));
    
    // stars for ratings
    $('.ratings').barrating({
        theme: 'bootstrap-stars',
        readonly : true
    });    
    
    $(document).on("submit", ".js-new-comment-form", function(e) {
        e.preventDefault();
        
        var dsid = GetTableRowID();
        if (Boolean(dsid) == false) dsid = 0;

        // block submit button
        var current_form = this;
        $(current_form).find("button").addClass("disabled");
        
        // hide messages
        $(current_form).find(".error-message").hide();
        $(current_form).find(".success-message").hide();

        // send registration request
        $.post(siteURL+'/', {
            action: 'comment',
            dsid: dsid,
            pid: $(current_form).find("input[name='pid']").val(),
            prognostics_use: $(current_form).find("select[name='prognostics_use']").val(),
            comment: $(current_form).find("textarea[name='new_comment_text']").val()
        }, function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                
                if (obj.error) {
                    // show error message
                    $(current_form).find(".error-message-text").html(obj.error_message);
                    $(current_form).find(".error-message").show();
                } else {
                    // show success message
                    $(current_form).find(".success-message-text").html(obj.message);
                    $(current_form).find(".success-message").show();
                    
                    // reset fields
                    $(current_form).find("textarea[name='new_comment_text']").val('');
                }

            } catch(error) {
                // show error message
                $(current_form).find(".error-message-text").html("Something is wrong with server's response");
                $(current_form).find(".error-message").show();
                
            }
            
            // enable submit button
            $(current_form).find("button").removeClass("disabled");
            
            // hide message after 5 secs
            setTimeout(function() {
                // hide messages
                $(current_form).find(".alert").hide();
                
                // hide form
                var pid = $(current_form).find("input[name='pid']").val();
                if (pid!==0&&pid!=='') $('#reply_form_placeholder_'+pid).html('');
            }, 5000);

        }).fail(function () {
            // can't reach server
            $(current_form).find("error-message-text").html("Can't connect to server");
            $(".error-message").show();

            $(current_form).find("button").removeClass("disabled");
        });
    });
    
    // comment reply 
    $(document).on("click", ".js-reply-link", function() {
        
        if (!user) {
            $("#js-login-dialog").click();
            return false;
        }
        
        var comment_id = $(this).attr('data-id');
        
        if ($('#reply_form_placeholder_'+comment_id).html()!=='') $('#reply_form_placeholder_'+comment_id).html('');
        else {
            
            // destroy/hide other forms
            $('.reply_form_placeholder').html('');
            
            $.get(siteURL + "/", {action:"prognostics"})
                .done(function (response) {
                    var prognostics_values = jQuery.parseJSON(response);
            
                    // show form
                    $('#reply_form_placeholder_'+comment_id).html(       
                            '<div class="col-md-12"><ul class="media-list">\
                                <li class="media">\
                                    <div class="media-left">\
                                        <img class="media-object avatar img-circle" src="http://www.gravatar.com/avatar/'+gravatar+'&s=120">\
                                    </div>\
                                    <div class="media-body">\
                                        <form class="js-new-comment-form">\
                                            <div class="alert alert-danger error-message">\
                                                <strong>Error: </strong><span class="error-message-text"></span>\
                                            </div>\
                                            <div class="alert alert-success success-message">\
                                                <strong>Ok! </strong><span class="success-message-text"></span>\
                                            </div>\
                                            <input name="pid" value="'+comment_id+'" type="hidden" />\
                                            <textarea name="new_comment_text" class="form-control" placeholder="Your Comment Text" required=""></textarea>\
                                            <select name="prognostics_use" class="selectpicker form-control" data-container="body" title="Choose prognostics use..."></select>\
                                            <button type="submit" class="btn btn-primary btn-block">Send</button>\
                                        </form>\
                                    </div>\
                                </li>\
                            </ul></div>');

                    autosize($('#reply_form_placeholder_'+comment_id+' textarea'));

                    $('#reply_form_placeholder_'+comment_id+" select").append($("<option />").val('').text(''));
                    $.each(prognostics_values, function() { 
                        $('#reply_form_placeholder_'+comment_id+" select").append($("<option />").val(this.ID).text(this.PrognosticsUseValue));
                    });
                    
                    $('.selectpicker').selectpicker('refresh');
            
                });
        } 
        
    });
    
    
    // like toggle
    $(document).on("click", ".js-like-toggle", function() {
        
        if (!user) {
            $("#js-login-dialog").click();
            return false;
        }
        
        var comment_id = $(this).attr('data-id');
        var current_link = $(this);
        
        
        // send registration request
        $.post(siteURL+'/users_controller.php', {
            action: 'like',
            id: comment_id
        }, function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                
                if (obj.error) {
                    // error - just do nothing
                    
                } else {
                    // 1. replace "Like" with "Liked"
                    var parent = current_link.parent();                   
                    
                    current_link.remove();
                    parent.prepend('Liked');
                    
                    // 2. update "Likes" value
                    $('#counted_likes_'+comment_id).html(obj.likes==1?'1 like':obj.likes+' likes');
                }

            } catch(error) {
                // show error message 
            }

        }).fail(function () {
            // can't reach server
        });
                        
    });
      
      
    $(document).on("click", ".js-comments-pagination", function() {
        if ($(this).parent().hasClass('disabled')) return false;
        load_user_comments($(this).attr('data-id'));
    });

	
	// Modal Dialog to show login/register 
    
    // retreive/show login dialog
    
    $(document).on("click", ".js-login-dialog", function () {		
        $.get(siteURL + "/login")
                .done(function (response) {
                    $("#headerModalLabel").html("Login");
                    $("#modalDialogContent").html(response);
                    $("#loginRegisterDialog").modal("show");
                });
    });
    

    // retreive/show registration dialog
    $(document).on("click", ".js-register-dialog", function () {

        $.get(siteURL + "/register")
                .done(function (response) {
                    $("#headerModalLabel").html("Registration");
                    $("#modalDialogContent").html(response);
                    $("#loginRegisterDialog").modal("show");
                });
    });

    // retreive/show forgot password dialog
    $(document).on("click", ".js-forgot-dialog", function () {


        $.get(siteURL + "/forgot_password")
                .done(function (response) {
                    $("#headerModalLabel").html("Forgot Password");
                    $("#modalDialogContent").html(response);
                    $("#loginRegisterDialog").modal("show");
                });
    });

    function get_prognostics_use() {
        $.get(siteURL + "/", {action:"prognostics"})
                .done(function (response) {
                    var prognostics_values = jQuery.parseJSON(response);

                    $("#new_comment select").append($("<option />").val('').text(''));
                    $.each(prognostics_values, function() {
                        $("#new_comment select").append($("<option />").val(this.ID).text(this.PrognosticsUseValue));
                    });
                    
                    $('.selectpicker').selectpicker('refresh');
            
                });
    }
    
    // load values
    get_prognostics_use();


    /* Registration form */
    
    $(document).on("show.bs.modal", "#loginRegisterDialog", function () {
        console.log('showing');
    });
	

    // submitting registration form
    $(document).on("submit", "#register-form", function (e) {
        // prevent from default actions
        e.preventDefault();

        // hide messages
        $(".error-message").hide();
        $(".success-message").hide();

        // block submit button
        $("#register-form button").addClass("disabled");

        // send registration request
        $.post(siteURL+'/users_controller.php', {
            action: 'registration',
            full_name: $('#full_name').val(),
            username: $('#username').val(),
            email: $('#email').val(),
            password: $('#password').val(),
            confirm_password: $('#confirm_password').val()
        }, function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                
                if (obj.error) {
                    // show error message
                    $("#js-error-message").html(obj.error_message);
                    $(".error-message").show();
                } else {
                    // show success message
                    $("#js-success-message").html(obj.message);
                    $(".success-message").show();
                    
                    // reset fields
                    $('#full_name').val('');
                    $('#username').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#confirm_password').val('');
                }

            } catch(error) {
                // show error message
                $("#js-error-message").html("Something is wrong with server's response");
                $(".error-message").show();
                
            }
            
            // enable submit button
            $("#register-form button").removeClass("disabled");

        }).fail(function () {
            // can't reach server
            $("#js-error-message").html("Can't connect to server");
            $(".error-message").show();

            $("#register-form button").removeClass("disabled");
        });

    });
    
    // logout process
    /*
    $(document).on("click", ".js-logout", function() {
        $.get(siteURL + "/logout")
                .done(function (response) {
                    // refresh current page
                    document.location = siteURL + "/";
                });
    });
*/
    
    // account
    $(document).on("submit", "#account-form", function(e) {
        // prevent default submit action
        e.preventDefault();
        
        // hide messages
        $(".error-message").hide();
        $(".success-message").hide();

        // block "submit" button
        $("#account-form button").addClass("disabled");
        
        // send data to server
        $.post(siteURL+'/users_controller.php', {
            action: 'update_account',
            full_name: $('#full_name').val(),
            password: $('#password').val(),
            confirm_password: $('#confirm_password').val()
        }, function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                
                if (obj.error) {
                    // show error
                    $("#js-error-message").html(obj.error_message);
                    $(".error-message").show();
                } else {
                    // show success message and reset form
                    $("#js-success-message").html(obj.message);
                    $(".success-message").show();
                    
                    // hide message(?)
                    setTimeout(function() {
                        $(".success-message").hide();
                    }, 3*1000);
                }
                
            } catch(error) {
                // show error
                $("#js-error-message").html("Something is wrong with server's response");
                $(".error-message").show();  
            }

            // enable submit button again
            $("#account-form button").removeClass("disabled");

        }).fail(function () {
            // can't reach server
            $("#js-error-message").html("Can't connect to server");
            $(".error-message").show();

            $("#account-form button").removeClass("disabled");
        });
    });
    
    // login process
    $(document).on("submit", "#login-form", function(e) {
        // prevent default submit action
        e.preventDefault();
        
        // hide messages
        $(".error-message").hide();
        $(".success-message").hide();

        // block "submit" button
        $("#login-form button").addClass("disabled");
        
        // send data to server
        $.post(siteURL+'/users_controller.php', {
            action: 'login',
            login: $('#username').val(),
            password: $('#password').val()
        }, function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                
                if (obj.error) {
                    // show error
                    $("#js-error-message").html(obj.error_message);
                    $(".error-message").show();
                } else {
                    // show success message and reset form
                    $("#js-success-message").html(obj.message);
                    $(".success-message").show();
                    
                    // refresh current page
                    document.location.reload(true);
                    
                }
                
            } catch(error) {
                // show error
                $("#js-error-message").html("Something is wrong with server's response");
                $(".error-message").show();
                
            }

            // enable submit button again
            $("#login-form button").removeClass("disabled");

        }).fail(function () {
            // can't reach server
            $("#js-error-message").html("Can't connect to server");
            $(".error-message").show();

            $("#login-form button").removeClass("disabled");
        });
        
    });
    
    // submitting new password form
    $(document).on("submit", "#new-password-form", function(e) {
        e.preventDefault();
        
        // hide messages
        $(".error-message").hide();
        $(".success-message").hide();

        // block "submit" button
        $("#new-password-form button").addClass("disabled");
        
        // send data to server
        $.post(siteURL+'/users_controller.php', {
            action: 'set-password',
            password1: $('#password1').val(),
            password2: $('#password2').val(),
            code:      $('#code').val() 
        }, function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                
                if (obj.error) {
                    // show error
                    $("#js-error-message").html(obj.error_message);
                    $(".error-message").show();
                } else {
                    // show success message and reset form
                    $("#js-success-message").html(obj.message);
                    $(".success-message").show();
                    
                    $('#username_email').val('');
                }
                
            } catch(error) {
                // show error
                $("#js-error-message").html("Something is wrong with server's response");
                $(".error-message").show();
                
            }

            // enable submit button again
            $("#new-password-form button").removeClass("disabled");

        }).fail(function () {
            // can't reach server
            $("#js-error-message").html("Can't connect to server");
            $(".error-message").show();

            $("#new-password-form button").removeClass("disabled");
        });
    });
    
    // submitting forgot password form
    $(document).on("submit", "#forgot-form", function(e) {
        e.preventDefault();
        
        // hide messages
        $(".error-message").hide();
        $(".success-message").hide();

        // block "submit" button
        $("#forgot-form button").addClass("disabled");
        
        // send data to server
        $.post(siteURL+'/users_controller.php', {
            action: 'reset-password',
            username_email: $('#username_email').val()
        }, function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                
                if (obj.error) {
                    // show error
                    $("#js-error-message").html(obj.error_message);
                    $(".error-message").show();
                } else {
                    // show success message and reset form
                    $("#js-success-message").html(obj.message);
                    $(".success-message").show();
                    
                    $('#username_email').val('');
                }
                
            } catch(error) {
                // show error
                $("#js-error-message").html("Something is wrong with server's response");
                $(".error-message").show();
                
            }

            // enable submit button again
            $("#forgot-form button").removeClass("disabled");

        }).fail(function () {
            // can't reach server
            $("#js-error-message").html("Can't connect to server");
            $(".error-message").show();

            $("#forgot-form button").removeClass("disabled");
        });
    });

	
	$("#registration-terms-of-use-checkbox").click(function() {
		if($(this)[0].checked == true) {
			$("#proceed-to-registration").removeClass("disabled");
		} else {
			$("#proceed-to-registration").addClass("disabled");
		}
		
		
	});

	$("#proceed-to-registration").click(function() {
		if(!$(this).hasClass("disabled")) {
			$("#registration-details").show();
			$("#registration-terms-of-use-wrapper").hide();
		}
	});
	
	
	
	
	
});
