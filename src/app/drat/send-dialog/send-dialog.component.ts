import { GoogleAnalyticsService } from '../../google-analytics.service';
import { AssessmentDetails } from '../types';
import { DratService } from '../drat.service';
import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatExpansionPanel } from '@angular/material/expansion';

@Component({
	selector: 'app-send-dialog.component',
	templateUrl: './send-dialog.component.html',
	styleUrls: ['./send-dialog.component.scss']
})
export class SendDialogComponent implements OnInit, AfterContentInit {
	responseMessage: string;
	responseOk: boolean;
	ready: number;
	assessment: AssessmentDetails;
	settingsExpand: boolean;

	constructor(
		public dialogRef: MatDialogRef<SendDialogComponent>,
		public service: DratService,
		public analytics: GoogleAnalyticsService
	) {}

	ngOnInit(): void {
		this.service.assessment$.subscribe(x => {
			this.assessment = x;
		});
		this.service.sendReportResponseMessage$.subscribe(x => {
			this.responseMessage = x;
		});
		this.service.sendReportResponseOk$.subscribe(x => {
			this.responseOk = x;
		});
		this.checkReady();
	}

	ngAfterContentInit(): void {
		if (this.ready > 0) {
			this.settingsExpand = true;
		}
	}

	onCancelClick(): void {
		this.dialogRef.close();
	}

	onSendClick() {
		this.analytics.emitEventUI('Send Report');
		this.service
			.sendReportResults
			// this.assessment.riskAssessorId.toString(),
			// this.assessment.requestId.toString(),
			// this.assessment.answers.toString(),
			// this.assessment.riskLevel.title.toString(),
			// this.assessment.riskLevel.minControls.toString(),
			// this.assessment.riskLevel.comments.toString()
			();
	}

	checkReady(): void {
		if (!this.assessment.returnAddress) {
			this.ready = 2;
		} else {
			if (!this.assessment.requestId || !this.assessment.riskAssessorId) {
				this.ready = 1;
			} else {
				this.ready = 0;
			}
		}
	}
}
