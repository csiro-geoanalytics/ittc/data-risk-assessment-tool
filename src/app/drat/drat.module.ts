import { DemoMaterialModule } from './../demo-material-module';
import { RiskReportComponent } from './risk-report/risk-report.component';
import { SendDialogComponent } from './send-dialog/send-dialog.component';
import { DataFormComponent } from './data-form/data-form.component';
import { MainContentComponent } from './main-content.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';

import { NgxPrintModule } from 'ngx-print';
import { DratService } from './drat.service';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MarkdownModule } from 'ngx-markdown';

const routes: Routes = [
	{
		path: '',
		component: MainContentComponent
	}
];

@NgModule({
	declarations: [
		QuestionnaireComponent,
		MainContentComponent,
		DataFormComponent,
		RiskReportComponent,
		SendDialogComponent
	],
	imports: [
		CommonModule,
		HttpClientModule,
		FlexLayoutModule,
		FormsModule,
		DemoMaterialModule,
		NgxPrintModule,
		MarkdownModule.forRoot(),
		RouterModule.forChild(routes)
	],
	exports: [],
	providers: [DratService],
	bootstrap: [MainContentComponent],
	entryComponents: [SendDialogComponent]
})
export class DratModule {
	constructor(private dateAdapter: DateAdapter<Date>) {
		dateAdapter.setLocale('en-au'); // DD/MM/YYYY
	}
}
