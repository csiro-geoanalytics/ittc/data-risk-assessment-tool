import { HttpClientModule } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";

import { DratService } from "./drat.service";
import { FlowChart } from "./types";

describe("AppService", () => {
	beforeEach(() =>
		TestBed.configureTestingModule({
			imports: [HttpClientModule]
		})
	);

	it("should be created", () => {
		const service: DratService = TestBed.get(DratService);
		expect(service).toBeTruthy();
	});

	it("should load json as flow chart", () => {
		const service: DratService = TestBed.get(DratService);
		let flowChart = null;
		service.getFlowChart().subscribe(x => {
			flowChart = x;
			expect(flowChart instanceof FlowChart).toBeTruthy();
		});

		expect(service).toBeTruthy();
	});
});
