import { Control, Question, FlowChart, Answer } from '../types';
import { DratService } from '../drat.service';
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, ViewContainerRef, ElementRef } from '@angular/core';
import { RiskLevel } from '../types';
import { MatDialog } from '@angular/material/dialog';

@Component({
	selector: 'app-questionnaire',
	templateUrl: './questionnaire.component.html',
	styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit, AfterViewInit {
	flowChart: FlowChart;
	activeQuestion: Question;
	activeControls: Control[] = [];
	riskLevel: RiskLevel;
	notes: String[] = [];
	riskAdjust = 0;
	answers: Answer[];
	assessment;
	riskLevelType: 'corporate' | 'personal';

	constructor(public service: DratService, private dialog: MatDialog) {
		this.initialise();
	}

	@ViewChild('helpDialog', { read: TemplateRef, static: true }) helpDialog: TemplateRef<any>;

	ngOnInit(): void {
		this.service.flowChart$.subscribe(x => {
			this.flowChart = x;
			if (x) {
				this.nextQuestion(0);
			}
		});

		this.service.assessment$.subscribe(x => {
			this.assessment = x;
		});
	}

	ngAfterViewInit() {}

	onSelectYes(): void {
		let numControls = 0;
		if (this.activeQuestion.yesControl) {
			numControls = this.activeQuestion.yesControl.length;
		}

		if (this.activeQuestion.yesRiskLevels) {
			this.riskLevelType = this.activeQuestion.yesRiskLevels;
		}

		this.addAnswer(
			new Answer(
				this.activeQuestion.question,
				'YES',
				this.activeQuestion.id,
				numControls,
				this.activeQuestion.yesRiskAdjust
			)
		);
		this.nextQuestion(this.activeQuestion.yes, this.activeQuestion.yesControl, this.activeQuestion.yesRiskAdjust);
	}

	onSelectNo(): void {
		let numControls = 0;
		if (this.activeQuestion.noControl) {
			numControls = this.activeQuestion.noControl.length;
		}

		if (this.activeQuestion.noRiskLevels) {
			this.riskLevelType = this.activeQuestion.noRiskLevels;
		}

		this.addAnswer(
			new Answer(
				this.activeQuestion.question,
				'NO',
				this.activeQuestion.id,
				numControls,
				this.activeQuestion.noRiskAdjust
			)
		);
		this.nextQuestion(this.activeQuestion.no, this.activeQuestion.noControl, this.activeQuestion.noRiskAdjust);
	}

	onSelectBack(): void {
		let lastAnswer = this.answers.pop();

		if (lastAnswer) {
			this.nextQuestion(lastAnswer.questionId);

			for (let i = 0; i < lastAnswer.numControls; i++) {
				this.activeControls.pop();
			}
		}

		if (this.riskLevel != null) {
			this.riskLevel = null;
		}

		if (lastAnswer.riskAdjust) this.riskAdjust -= lastAnswer.riskAdjust;
	}

	onSelectDunno(): void {
		this.dialog.open(this.helpDialog);
	}

	private addAnswer(answer: Answer) {
		this.answers.push(answer);
	}

	private getHelpText() {
		let helpText = '';
		if (this.activeQuestion.help) {
			helpText = this.flowChart.help.find(x => x.id === this.activeQuestion.help).text.toString();
		}
		return helpText;
	}

	initialise(): void {
		this.activeControls = [];
		this.riskLevel = null;
		// this.notes = [];
		this.riskAdjust = 0;
		this.answers = [];
	}

	onRestart(): void {
		this.initialise();
		this.nextQuestion(0);
	}

	generateReport() {
		this.service.generateReport(this.riskLevel, this.answers, this.activeControls);
	}

	nextQuestion(id: any, controlId?: number[], riskAdjust?: number): void {
		if (controlId) {
			controlId.forEach(cId => {
				const newControl = this.flowChart.controls.find(x => x.id === cId);
				if (newControl) this.activeControls.push(newControl);
				else console.log(`Error control with id ${cId} does not exist`);
			});
		}

		if (riskAdjust) this.riskAdjust += riskAdjust;

		if (isNaN(id)) {
			let rLevels: RiskLevel[];
			let rl: RiskLevel;

			// detirmine which risklevels array to use
			if (this.riskLevelType === 'corporate') {
				rLevels = this.flowChart.corporate;
			} else {
				rLevels = this.flowChart.personal;
			}

			rl = rLevels.find(x => x.id === id);

			// if risk level is found check for any adjustments
			if (rl) {
				// this applies a floor risk level of 0 to the final assessment
				let adjustedLevel = rl.level + this.riskAdjust;
				if (adjustedLevel < 0) adjustedLevel = 0;

				this.riskLevel = rLevels.find(x => x.level === adjustedLevel);
			}
			// if risk level not found something has gone really wrong
			else {
				this.riskLevel = rLevels.find(x => x.level === -2);
			}

			this.activeQuestion = null;
		} else {
			this.activeQuestion = this.flowChart.questions.find(x => x.id === id);
		}
	}
}
