import { GoogleAnalyticsService } from '../../google-analytics.service';
import { AssessmentDetails } from '../types';
import { DratService } from '../drat.service';
import { SendDialogComponent } from '../send-dialog/send-dialog.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import * as jspdf from 'jspdf';

@Component({
	selector: 'app-risk-report',
	templateUrl: './risk-report.component.html',
	styleUrls: ['./risk-report.component.scss']
})
export class RiskReportComponent implements OnInit {
	assessment: AssessmentDetails;

	margins = {
		top: 70,
		bottom: 40,
		left: 30,
		width: 800
	};
	base64Img = null;

	constructor(public service: DratService, public analytics: GoogleAnalyticsService, public dialog: MatDialog) {
		this.imgToBase64('assets/drat.png', base64 => {
			this.base64Img = base64;
		});
	}

	ngOnInit(): void {
		this.service.assessment$.subscribe(x => {
			this.assessment = x;
		});
	}

	backToQuestions() {
		this.service.setToggleReport(Boolean(false));
	}

	print() {
		this.analytics.emitEventUI('Print Report');
		window.print();
	}

	public printAssessment() {
		return JSON.stringify(this.assessment);
	}

	openSendDialog(): void {
		const dialogRef = this.dialog.open(SendDialogComponent, {
			width: '750px'
		});

		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed');
		});
	}

	public generatePDF() {
		let data = document.getElementById('pdf-content');
		let pdf = new jspdf('p', 'pt', 'a4'); // A4 size page of PDF
		pdf.setFontSize(15);
		pdf.fromHTML(
			data,
			this.margins.left, // x coord
			this.margins.top,
			{
				// y coord
				width: this.margins.width // max width of content on PDF
			},
			dispose => {
				this.headerFooterFormatting(pdf);
			},
			this.margins
		);

		let iframe = document.createElement('iframe');
		iframe.setAttribute('style', 'height:100%; width:100%;');

		let pdfWindow = window.open('');
		pdfWindow.document.body.setAttribute('style', 'margin:0; overflow:hidden;');
		pdfWindow.document.body.appendChild(iframe);
		iframe.src = pdf.output('datauristring');
	}

	private headerFooterFormatting(doc) {
		let totalPages = doc.internal.getNumberOfPages();

		for (let i = totalPages; i >= 1; i--) {
			// make this page, the current page we are currently working on.
			doc.setPage(i);

			this.header(doc);

			this.footer(doc, i, totalPages);
		}
	}

	private header(doc) {
		doc.setFontSize(30);
		doc.setTextColor(40);
		doc.setFontStyle('normal');

		if (this.base64Img) {
			doc.addImage(this.base64Img, 'JPEG', this.margins.left, 10, 40, 40);
		}

		doc.text('Data Risk Assessment', this.margins.left + 50, 40);

		doc.line(3, 70, this.margins.width + 43, 70); // horizontal line
	}

	private imgToBase64(url, callback) {
		let xhr = new XMLHttpRequest();
		xhr.responseType = 'blob';
		xhr.onload = function() {
			let reader = new FileReader();
			reader.onloadend = () => {
				let resultStr = reader.result.toString();
				let imgVariable = resultStr.replace('text/xml', 'image/png');
				callback(imgVariable);
			};
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.send();
	}

	private footer(doc, pageNumber, totalPages) {
		let str = 'Page ' + pageNumber + ' of ' + totalPages;
		doc.setFontSize(10);
		doc.text(str, this.margins.left, doc.internal.pageSize.height - 20);
	}
}
