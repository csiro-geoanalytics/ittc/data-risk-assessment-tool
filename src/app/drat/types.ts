export class Question {
	public constructor(
		public id: number,
		public question: String,
		public yes: number,
		public no: number,
		public yesControl?: number[],
		public noControl?: number[],
		public yesRiskAdjust?: number,
		public noRiskAdjust?: number,
		public yesRiskLevels?: 'corporate' | 'personal',
		public noRiskLevels?: 'corporate' | 'personal',
		public note?: String,
		public help?: number
	) {}
}

export class Control {
	public constructor(public id: number, public text: String, public actor?: String) {}
}

export class Help {
	public constructor(public id: number, public text: String) {}
}

export class RiskLevel {
	public constructor(
		public id: number,
		public level: number,
		public title: String,
		public text?: String,
		public riskFrom?: String,
		public comments?: String,
		public minControls?: MinControls
	) {}
}

export class MinControls {
	safeProjects: String;
	safePeople: String;
	safeSettings: String;
	safeOutputs: String;
}

export class FlowChart {
	public constructor(
		public questions: Question[],
		public controls: Control[],
		public personal: RiskLevel[],
		public corporate: RiskLevel[],
		public help: Help[]
	) {}
}

export class Answer {
	public constructor(
		public question: String,
		public answer: String,
		public questionId: number,
		public numControls: number,
		public riskAdjust: number
	) {}
}

export class AssessmentDetails {
	riskAssessorName: string;
	riskAssessorPosition: string;
	riskAssessorEmail: string;
	riskAssessorId: string;
	company: string;
	date: Date;
	projectName: string;
	dataDetails: string;
	dataSource: string;
	requestorName: string;
	requestorEmail: string;
	requestId: string;
	requestDescription: string;
	riskLevel: RiskLevel;
	answers: Answer[];
	controls: Control[];
	returnAddress: string;

	constructor() {
		this.date = new Date();
	}
}
