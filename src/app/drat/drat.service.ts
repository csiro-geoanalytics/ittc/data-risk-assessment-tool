import { FlowChart, AssessmentDetails, RiskLevel, Answer, Control } from './types';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

@Injectable()
export class DratService {
	public assessment$: BehaviorSubject<AssessmentDetails>;
	public toggleReport$: BehaviorSubject<boolean>;
	public flowChart$: BehaviorSubject<FlowChart>;
	public sendReportResponseOk$: BehaviorSubject<boolean>;
	public sendReportResponseMessage$: BehaviorSubject<string>;

	constructor(private http: HttpClient) {
		this.assessment$ = new BehaviorSubject<AssessmentDetails>(null);
		this.toggleReport$ = new BehaviorSubject<boolean>(false);
		this.flowChart$ = new BehaviorSubject<FlowChart>(null);
		this.sendReportResponseOk$ = new BehaviorSubject<boolean>(null);
		this.sendReportResponseMessage$ = new BehaviorSubject<string>(null);
	}

	public generateReport(riskLevel: RiskLevel, answers: Answer[], controls: Control[]) {
		let assess = this.assessment$.getValue();
		assess.riskLevel = riskLevel;
		assess.answers = answers;
		assess.controls = controls;
		this.assessment$.next(assess);
		this.toggleReport$.next(true);
	}

	public getFlowChart(): Observable<FlowChart> {
		return this.http.get<FlowChart>('./assets/flow-chart.json');
	}

	public setToggleReport(toggle) {
		this.toggleReport$.next(toggle);
	}

	public sendReportResults() {
		const httpOptions = {
			headers: new HttpHeaders({
				'Access-Control-Allow-Origin': '*',
				'Content-type': 'application/json'
			})
		};

		// let formData: any = new FormData();
		let assess = this.assessment$.getValue();
		if (assess.returnAddress) {
			this.http.post<HttpResponse<any>>(assess.returnAddress, JSON.stringify(assess), httpOptions).subscribe(
				response => {
					console.log(response);
					this.sendReportResponseOk$.next(true);
					this.sendReportResponseMessage$.next(response['message']);
				},
				error => {
					console.log(error);
					this.sendReportResponseOk$.next(error.ok);
					if (error.error.detail) {
						this.sendReportResponseMessage$.next(error.message + ' : ' + error.error.detail);
					} else {
						this.sendReportResponseMessage$.next(error.message);
					}
				}
			);
		}
	}
}
