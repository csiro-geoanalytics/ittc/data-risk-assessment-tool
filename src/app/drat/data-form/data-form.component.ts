import { DratService } from '../drat.service';
import { AssessmentDetails } from '../types';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-data-form',
	templateUrl: './data-form.component.html',
	styleUrls: ['./data-form.component.scss']
})
export class DataFormComponent implements OnInit {
	data: AssessmentDetails;
	@Input() riskForm: FormGroup;

	constructor(public service: DratService, private readonly route: ActivatedRoute) {
		this.data = new AssessmentDetails();
	}

	ngOnInit() {
		let assess = this.service.assessment$.getValue();
		if (assess) {
			this.data = assess;
		} else {
			this.data.riskAssessorName = this.route.snapshot.queryParamMap.get('riskAssessorName');
			this.data.riskAssessorPosition = this.route.snapshot.queryParamMap.get('riskAssessorPosition');
			this.data.riskAssessorEmail = this.route.snapshot.queryParamMap.get('riskAssessorEmail');
			this.data.riskAssessorId = this.route.snapshot.queryParamMap.get('riskAssessorId');
			this.data.company = this.route.snapshot.queryParamMap.get('company');
			this.data.requestorName = this.route.snapshot.queryParamMap.get('requestorName');
			this.data.requestorEmail = this.route.snapshot.queryParamMap.get('requestorEmail');
			this.data.requestDescription = this.route.snapshot.queryParamMap.get('requestDescription');
			this.data.requestId = this.route.snapshot.queryParamMap.get('requestId');
			this.data.projectName = this.route.snapshot.queryParamMap.get('projectName');
			this.data.dataSource = this.route.snapshot.queryParamMap.get('dataSource');
			this.data.dataDetails = this.route.snapshot.queryParamMap.get('dataDetails');
			this.data.returnAddress = this.route.snapshot.queryParamMap.get('returnAddress');
		}
	}

	onSubmit() {
		this.service.assessment$.next(this.data);
		this.gotoTop();
	}

	gotoTop() {
		let el = document.getElementById('page-content');
		el.scrollTo(0, 0);
	}

	resetForm() {
		this.data = new AssessmentDetails();
	}
}
