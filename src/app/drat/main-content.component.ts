import { DratService } from "./drat.service";
import { Component, OnInit } from "@angular/core";
import { environment } from "../../environments/environment";

@Component({
	selector: "app-main-content",
	templateUrl: "./main-content.component.html",
	styleUrls: ["./main-content.component.scss"]
})
export class MainContentComponent implements OnInit {
	showReportToggle = false;

	env = environment;

	constructor(public service: DratService) {}

	ngOnInit() {
		this.service.toggleReport$.subscribe(x => {
			this.showReportToggle = x;
		});

		this.service.getFlowChart().subscribe(data => {
			this.service.flowChart$.next(data);
		});
	}
}
