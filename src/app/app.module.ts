import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';

@NgModule({
	declarations: [AppComponent],
	imports: [BrowserModule, HttpClientModule, BrowserAnimationsModule, AppRoutingModule],
	providers: [{ provide: 'GA_TRACKING_ID', useValue: environment.googleAPI.ga }],
	bootstrap: [AppComponent]
})
export class AppModule {
	constructor() {}
}
