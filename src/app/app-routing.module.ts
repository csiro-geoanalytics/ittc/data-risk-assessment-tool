import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const assessRoutes: Routes = [
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	{
		path: 'home',
		loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
	},
	{
		path: 'assessment',
		loadChildren: () => import('./drat/drat.module').then(m => m.DratModule)
	},
	{ path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
	imports: [RouterModule.forRoot(assessRoutes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
