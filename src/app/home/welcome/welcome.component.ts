import { GoogleAnalyticsService } from '../../google-analytics.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-welcome',
	templateUrl: './welcome.component.html',
	styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
	constructor(public analytics: GoogleAnalyticsService) { }

	ngOnInit() { }

	readPaper() {
		this.analytics.emitEventUI('Download paper');
	}

	viewFlowChart() {
		this.analytics.emitEventUI('View Flowchart');
	}
}
