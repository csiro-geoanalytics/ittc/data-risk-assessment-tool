import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
	{
		path: '',
		component: WelcomeComponent
	}
];

@NgModule({
	declarations: [WelcomeComponent],
	imports: [CommonModule, RouterModule.forChild(routes)]
})
export class HomeModule {}
