import { Injectable, Inject } from '@angular/core';

import { fromEvent } from 'rxjs';

import 'autotrack';
import 'autotrack/lib/plugins/clean-url-tracker';
import 'autotrack/lib/plugins/event-tracker';
import 'autotrack/lib/plugins/outbound-link-tracker';
import 'autotrack/lib/plugins/url-change-tracker';

import * as _ from 'lodash';

@Injectable({
	providedIn: 'root'
})
export class GoogleAnalyticsService {
	private _disabled: boolean;

	constructor(
		@Inject('GA_TRACKING_ID')
		private GA_TRACKING_ID: string
	) {
		if (!this.GA_TRACKING_ID) {
			this._disabled = true;
			console.warn('GA_TRACKING_ID is not provided for the GoogleAnalyticsService. Service is disabled.');
			return;
		} else {
			this.setEnabled(true);
		}

		// Inject Google Analytics script.
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			(i[r] =
				i[r] ||
				function() {
					(i[r].q = i[r].q || []).push(arguments);
				}),
				(i[r].l = +new Date());
			(a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m);
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		// Configure tracker.
		ga('create', this.GA_TRACKING_ID, 'auto');
		ga('require', 'eventTracker');
		ga('require', 'outboundLinkTracker');
		ga('require', 'urlChangeTracker');
		ga('send', 'pageview');

		// Check if tracker is loaded.
		fromEvent(window, 'load').subscribe(() => this.detectAdBlocker());
	}

	get enabled() {
		return this._disabled;
	}

	set enabled(value: boolean) {
		if (!this.GA_TRACKING_ID) return;
		if (!value) this.emitEventConfiguration('Google Analytics', 'disabled');
		this.setEnabled(value);
		if (value) this.emitEventConfiguration('Google Analytics', 'enabled');
	}

	private setEnabled(value: boolean) {
		if (this.GA_TRACKING_ID) window['ga-disable-' + this.GA_TRACKING_ID] = this._disabled = !value;
	}

	private detectAdBlocker() {
		if (!this.GA_TRACKING_ID) return;
	}

	emitEvent(category: string, action: string, label: string, value?: number) {
		if (!this.enabled) return;
		ga('send', 'event', {
			eventCategory: category,
			eventAction: action,
			eventLabel: label,
			eventValue: value
		});
	}

	emitEventUI(action: string, label?: string, value?: number) {
		this.emitEvent('UI', action, label, value);
	}

	emitEventConfiguration(action: string, label?: string, value?: number) {
		this.emitEvent('Configuration', action, label, value);
	}

	emitEventData(action: string, label?: string, value?: number) {
		this.emitEvent('Data', action, label, value);
	}

	emitEventDashboard(action: string, label?: string, value?: number) {
		this.emitEvent('Dashboard', action, label, value);
	}

	/*
		User timings.
		https://developers.google.com/analytics/devguides/collection/analyticsjs/user-timings
	*/

	startTiming(category: string, variable: string, label?: string): TimingHandle {
		const start = _.now();
		return <TimingHandle>{
			end: () =>
				this.emitTiming(<TimingDescriptor>{
					category: category,
					variable: variable,
					label: label,
					start: start
				})
		};
	}

	private emitTiming(handle: TimingDescriptor): number {
		if (!this.enabled) return null;
		const time = _.now() - handle.start;
		ga('send', 'timing', handle.category, handle.variable, time, handle.label);
		return time;
	}

	startTimingData(variable: string, label?: string) {
		return this.startTiming('Data', variable, label);
	}

	startTimingDashboard(variable: string, label?: string) {
		return this.startTiming('Dashboard', variable, label);
	}

	startTimingInversionDashboard(variable: string, label?: string) {
		return this.startTiming('Inversion Dashboard', variable, label);
	}
}

/*
	User Timings handles.
*/

export interface TimingDescriptor {
	category: string;
	variable: string;
	label?: string;
	start: number;
}

export interface TimingHandle {
	end: () => number;
}
