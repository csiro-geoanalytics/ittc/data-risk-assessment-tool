// Global declarations.
declare var jQuery: any;

// Google Analytics.
declare var ga: Function;

/**
 *	Toastr.
 */

declare let toastr: Toastr;

declare interface Toastr
{
	version: string;
	options: Partial<ToastrOptions>;

	success(message: string, title?: string, optionsOverride?: Partial<ToastrOptions>);
	info(message: string, title?: string, optionsOverride?: Partial<ToastrOptions>);
	warning(message: string, title?: string, optionsOverride?: Partial<ToastrOptions>);
	error(message: string, title?: string, optionsOverride?: Partial<ToastrOptions>);

	clear(toastElement: any, clearOptions?: any);
	remove(toastElement: any);
	getContainer(options?: any, create?: boolean): any;
	subscribe(callback: (args) => void);
}

declare interface ToastrOptions
{
	tapToDismiss		: boolean;
	toastClass			: string;
	debug				: boolean;

	showMethod			: string;		// fadeIn; slideDown; and show are built into jQuery
	showDuration		: number;
	showEasing			: string;		// swing and linear are built into jQuery
	hideMethod			: string;
	hideDuration		: number;
	hideEasing			: string;
	closeButton			: boolean;
	closeMethod			: boolean;
	closeDuration		: boolean;
	closeEasing			: boolean;

	onclick				: (event: MouseEvent) => void;

	timeOut				: number;		// Set timeOut and extendedTimeOut to 0 to make it sticky
	extendedTimeOut		: number;

	iconClass			: string;
	positionClass		: string;
	titleClass			: string;
	messageClass		: string;
	escapeHtml			: boolean;
	closeHtml			: string;
	newestOnTop			: boolean;
	preventDuplicates	: boolean;
	progressBar			: boolean;
}
