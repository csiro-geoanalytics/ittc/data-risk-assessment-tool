// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	firebaseConfig: {
		apiKey: 'AIzaSyC5Me5_Z2JUwmwJGWYCefEjA125KxY9sgg',
		authDomain: 'gp-toolkit-staging.firebaseapp.com',
		databaseURL: 'https://gp-toolkit-staging.firebaseio.com',
		projectId: 'gp-toolkit-staging',
		storageBucket: 'gp-toolkit-staging.appspot.com',
		messagingSenderId: '63677556421',
		appId: '1:63677556421:web:ae779521451a51ae'
	},
	googleAPI: {
		ga: 'UA-148149402-3'
	}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
