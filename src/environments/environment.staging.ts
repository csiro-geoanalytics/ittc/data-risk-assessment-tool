export const environment = {
	production: true,
	firebaseConfig: {
		apiKey: 'AIzaSyC5Me5_Z2JUwmwJGWYCefEjA125KxY9sgg',
		authDomain: 'gp-toolkit-staging.firebaseapp.com',
		databaseURL: 'https://gp-toolkit-staging.firebaseio.com',
		projectId: 'gp-toolkit-staging',
		storageBucket: 'gp-toolkit-staging.appspot.com',
		messagingSenderId: '63677556421',
		appId: '1:63677556421:web:ae779521451a51ae'
	},
	googleAPI: {
		ga: 'UA-148149402-3'
	}
};
