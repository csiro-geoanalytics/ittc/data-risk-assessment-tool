export const environment = {
	production: true,
	firebaseConfig: {
		apiKey: 'AIzaSyCp8gxC4FWd1lj5u0vLh7QwT8Q4qT_jL-Y',
		authDomain: 'data-risk-analysis-tool.firebaseapp.com',
		databaseURL: 'https://data-risk-analysis-tool.firebaseio.com',
		projectId: 'data-risk-analysis-tool',
		storageBucket: 'data-risk-analysis-tool.appspot.com',
		messagingSenderId: '969650166395',
		appId: '1:969650166395:web:c5c6796022e045a40420de',
		measurementId: 'G-9V8G4P2GQJ'
	},
	// Google APIs.
	googleAPI: {
		ga: "UA-148149402-3"
	},
};
